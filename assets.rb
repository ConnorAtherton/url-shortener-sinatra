module UrlShortener
  class Assets < Sinatra::Application
    set :assets, Sprockets::Environment.new

    assets.append_path "assets/stylesheets"
    assets.append_path "assets/javascripts"

    assets.css_compressor = :scss

    get "/assets/*" do
      env["PATH_INFO"].sub!("/assets", "")
      settings.assets.call(env)
    end
  end
end
