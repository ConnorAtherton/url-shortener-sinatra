$LOAD_PATH << File.expand_path('../', __FILE__)

require 'rubygems'
require 'bundler'

Bundler.require

require 'assets'
require 'database'

module UrlShortener
  class Application < Sinatra::Base
    use Assets
    register Sinatra::Flash
    register Sinatra::Reloader

    helpers do
      def h(text)
        Rack::Utils.escape_html(text)
      end

      def root_url
        "http://#{headers['SERVER_NAME'] || 'localhost:3000'}"
      end

      def from_root_url(text)
        root_url + '/' + text.to_s
      end

    end

    get '/' do
      @links = Link.all
      @link = Link.new

      haml :index
    end

    post '/links' do
      # Check if we have already seen this before
      @link = Link.where(original_link: params[:link][:original_link]).first

      if @link
        haml :show
      else
        if @link = Link.create(params[:link])
          flash[:notice] = 'Created a new link'
          redirect "links/#{@link.code}"
        else
          flash.now[:error] = 'Creation failed'
          haml :index
        end
      end
    end

    get '/links/:code' do
      @link = Link.where(code: params[:code]).first

      haml :show
    end

    get '/:code' do
      @link = Link.where(code: params[:code]).first

      @link.increase_used_count

      redirect @link.original_link
    end
  end
end
